﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Drawing;
using System.Drawing.Imaging;
using System;
using System.IO;

public class MainMenu : MonoBehaviour {

    Image gif;

    public static System.Drawing.Image Texture2Image(Texture2D texture)
    {
        if (texture == null)
        {
            return null;
        }
        byte[] bytes = texture.EncodeToPNG();

        MemoryStream ms = new MemoryStream(bytes);

        ms.Seek(0, SeekOrigin.Begin);

        System.Drawing.Image bmp2 = System.Drawing.Bitmap.FromStream(ms);

        ms.Close();
        ms = null;

        return bmp2;
    }

    void Start () {
        TextAsset textAsset = (TextAsset)Resources.Load("g1", typeof(TextAsset));
        byte[] a = textAsset.bytes;
        MemoryStream ms = new MemoryStream(a);

        ms.Seek(0, SeekOrigin.Begin);
        gif = Image.FromStream(ms);
        gifImage = new GifImage(gif);
    }
	
	// Update is called once per frame
	void Update () {
	}

    public Texture2D tex;
    GifImage gifImage;
    Texture2D frame;
    Image currentFrame;

    public static Texture2D ImageToTexture(System.Drawing.Image im)
    {
        if (im == null)
        {
            return new Texture2D(4, 4);
        }
        MemoryStream ms = new MemoryStream();
        im.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
        ms.Seek(0, SeekOrigin.Begin);
        Texture2D tex = new Texture2D(im.Width, im.Height); 
        tex.LoadImage(ms.ToArray());
        ms.Close();
        ms = null;

        //
        return tex;
    }
    int it = 0;
    void OnGUI()
    {
        currentFrame = gifImage.GetFrame(it);
        frame = ImageToTexture(currentFrame);
        it++;
        GUI.DrawTexture(new Rect(0, 0, 100, 100), frame);
        currentFrame = gifImage.GetNextFrame();

    }
}
